**Level making is fun**, but the set-up is still a bit complicated. I hope this instruction helps.

With this instruction you can create new levels for Apple Flinger. You can also fix existing ones, add new episodes or make your complete own version of the game (if you create enough levels). I explain it **without the use of any IDE**. Optionally you can set up Eclipse or whatever you like.

There is something like a level-editor, but it is still all done in source code. Basic programming skills are an advantage. Also the level-editor is not extremely user-friendly, but it works.

# make the desktop version run
You cant create new levels on the phone, do it on your computer. I'm running Ubuntu Linux. I have Java and grade installed. After checking out the code I can run the desktop version of the game like this:
``scripts/run-desktop.sh``
or run the content of that script, which is:
```
./gradlew desktop:dist # desktop:run
java -jar desktop/build/libs/desktop-1.0.jar
```
If the game starts. It is all good. you can close it again.

# Enable sandbox mode
This is the mode that allows you to edit levels. You can't play the game in this mode. Open the file ``./core/src/com/gitlab/ardash/appleflinger/global/GameManager.java`` and find this line:

```
	public static final boolean SANDBOX = false;
```
Then change it to this line: 
```
	public static final boolean SANDBOX = true;
```

Then run the game again, open any level and click with the mouse a few times across on the screen. You should see wooden parts dropping down where ever you click. If they don't fall down, start the physics by pressing `O` on your keyboard.
![image](uploads/32ea1033e8180401322d554f28e1e3a7/image.png)

Congratulations! You are now in edit-mode and can change that level.

If you want to open any other level (than level 1), you can enable them all by setting `ALLLEVELS` also to `true`. We need that for adding a new level so change:
```
	public static final boolean ALLLEVELS = false;
```
to: 
```
	public static final boolean ALLLEVELS = true;
```

# General remarks on creating levels
1.  You build only the left side of the level. The right side, will be created by the game later (it is mirrored).
2.  The description language is Java, but you don't have to write it (mostly). You build your level with the mouse and then press `P`. That will print the source code four your level.
3.  Each level is in a class, check the existing level, if you like, to see how they are made.
4.  A level is called `Mission` in in the source code.
5.  Each `Mission` belongs to an *episode*. Currently I have 2 *episodes*: `original` and `winter`.
6.  The position of the sling-shot, must be entered manually. The wood pieces and the enemies are created by the level editor.
7.  There is limited space on the screen. unlike Angry Birds the scene is very small. And both sides have to fit on the screen. The slingshot should not be too close the own or to the enemy base.

# Create an empty level
To make a new level, you simply copy and existing one, and remove it's content. You can see all existing ones with
```
ls core/src/com/gitlab/ardash/appleflinger/missions/Mission*
```
The latrest one that I have on my computer is 
```
core/src/com/gitlab/ardash/appleflinger/missions/MissionM_2_6.java
```

In that file name the `2` is the Episode and `6` is the number of the mission. This file contains the last mission of episode 2 (*winter*).

In this tutorial I want to add another level to *winter*, so I copy the last one:
```
cp core/src/com/gitlab/ardash/appleflinger/missions/MissionM_2_6.java core/src/com/gitlab/ardash/appleflinger/missions/MissionM_2_7.java
```

In Java the name of the class and the name of the file must be the same. So change the class name in the file also  so MissionM_2_7. On a Linux PC this will do it:
```
sed -i 's/MissionM_2_6/MissionM_2_7/' core/src/com/gitlab/ardash/appleflinger/missions/MissionM_2_7.java
```


All missions and episodes are listed in `Mission.java` so the game can correctly detect the end of an episode. I add my new mission there. So in `./core/src/com/gitlab/ardash/appleflinger/missions/Mission.java` I change:
```
	    M_2_6,
	    END_OF_EPISODE_2,
```
to
```
	    M_2_6,
	    M_2_7,
	    END_OF_EPISODE_2,
```

If I start the game now. I will see a new mission (it will look like the other one that I copied it from). So I want to check that, so I run:
```
scripts/run-desktop.sh
```
again and see:
![image](uploads/181cc0111cc582981d38c5938a794cef/image.png)

To remove all the content, you simply delete all the lines that contain the word `WOOD` or `TARGET` (except one) in you new `MissionM_2_7.java` file. That just leaves the `Slingshot` and the small `STONE` under the slingshot. 

The class in my empty level file looks now like this:
```
public class MissionM_2_7 implements StageFiller{
	
	@Override
	public Group fillMirrorStage(GameWorld world) {
		Group group = new Group();
		
		// sling holder
		final float sx = 1.7457057f;
		final float sy = 1.9315788f;
		group.addActor( new BlockActor(world,MaterialConfig.STONE,sx, sy,0.3f,0.1f, BodyType.StaticBody)); 
		
		SlingShotActor slingshot1 = new SlingShotActor(sx, sy);
		group.addActor(slingshot1);
		
		return group;
	}

}
```
And on the screen, when I load it in the game, it looks like this:
![image](uploads/c3ab901a3f4437ee9c189fa082cd1c51/image.png)

# move the slingshot
The whole scene is about 7 units wide and 5 units high. In the `MissionM_2_7.java` there are the variables `sx` and `sy`  they define the position of the slingshot. Here you just play with the numbers a bit, then restart the game to see the result. I want my slingshot slight higher and slightly more to the center. So I increase both numbers:
```
		final float sx = 2.7f;
		final float sy = 2.9f;
```
And I want to have the stone under the slingshot a bit longer, so I change the `0.3` to `0.4`
```
		group.addActor( new BlockActor(world,MaterialConfig.STONE,sx, sy,0.4f,0.1f, BodyType.StaticBody)); 
```

Restart the game and see how it looks like:
![image](uploads/2fe92053c1c369b0c4036767c37a5578/image.png)

# place all wood and enemies
**This is the fun part**. Apart from clicking with the mouse you can use these keys on your keyboard:
*  `1,2,3,4,5,6,7,8,9,W` to switch between wood pieces or enemies. The currently selected is shown on the top right of the screen.
*  `O`: **start** the physics (press this once before you start)
*  `R`: to **rotate** the current part by 90 degrees
*  to **rotate** a part a different degree (like 45) you have to create another part that lays under it and then lay the new part gently on top of it, so it gently falls down.
*  `P`: to **print** the result (the print will go into your source code)
*  to **delete** an element just drop another element on top of it (level editing works with all physics enabled)
*  to **clear** the whole scene, quit to menu and open it again
*  **draw only on the left**

Here is what I made quickly:
![image](uploads/95454c1503ba4c475168303432df2566/image.png)

To save your current state you can press `P` any time. It will print the source code description of your current scene to the command line.

# save your file
When you have finished press `P` to print the source to the CLI. I have created my final version like this:
![image](uploads/bdc26fba5fca330b7c80bec66a6275f8/image.png)

In the command line interface (CLI), each line represents a piece of wood or an enemy. Copy all lines from there and paste them into your `MissionM_2_7.java`. I also reordered the lines, to put all enemies at the bottom, so they will be rendered last. My class looks now like this (you don't need to read that code):

```
public class MissionM_2_7 implements StageFiller{
	
	@Override
	public Group fillMirrorStage(GameWorld world) {
		Group group = new Group();
		
		// sling holder
		final float sx = 2.7f;
		final float sy = 2.9f;
		group.addActor( new BlockActor(world,MaterialConfig.STONE,sx, sy,0.4f,0.1f, BodyType.StaticBody)); 
		
		SlingShotActor slingshot1 = new SlingShotActor(sx, sy);
		group.addActor(slingshot1);
		
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_42, 2.502997f, 0.327518f, -1.5711077f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_42, 3.1652284f, 0.3273088f, -1.5710667f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_42, 3.807771f, 0.32673874f, -1.5710825f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_42, 4.369137f, 0.32713062f, -1.5699047f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_42, 4.8814545f, 0.32765782f, -1.5692266f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_42, 5.406339f, 0.32766584f, -1.5720624f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 5.060024f, 0.6711216f, 0.00210797f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 3.9824853f, 0.6685643f, 0.0021003406f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 2.9151483f, 0.6702248f, -0.0011869749f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 3.0328777f, 0.8750949f, -0.0012632688f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 4.911217f, 0.8764472f, 0.0026343982f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 4.638854f, 0.87577945f, 0.0023473422f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 3.3123474f, 0.8747739f, -0.0012365659f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 3.0323076f, 1.1393794f, -7.6640444E-4f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 3.306112f, 1.139201f, -4.6218236E-4f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 4.6330647f, 1.1407079f, 0.002477042f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_22, 4.9114532f, 1.1414211f, 0.0026153247f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_41, 3.6446638f, 1.0060679f, -1.5698776f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_41, 4.2895923f, 1.0073769f, -1.5737171f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 3.4110003f, 1.3443705f, -5.0226636E-6f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 4.49403f, 1.3461806f, 0.0020587286f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 3.959953f, 1.4896601f, 0.0024192175f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 2.890399f, 1.4906138f, -0.002507464f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_RECT, 4.4252825f, 1.833611f, 0.007012113f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 5.0344872f, 1.4942507f, 0.004948362f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_RECT, 3.3897707f, 1.8315976f, -0.006730334f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_11, 4.2224717f, 2.1667259f, 0.010050518f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_11, 3.5954747f, 2.1634378f, -0.0011494318f, BodyType.DynamicBody));
		group.addActor (new BlockActor(world, MaterialConfig.WOOD_BL_81, 3.8817766f, 2.300305f, 0.005928739f, BodyType.DynamicBody));
		group.addActor (new PengActor(world, MaterialConfig.TARGET_PENG, 2.6464355f, 0.9916306f, 0.5f));
		group.addActor (new PengActor(world, MaterialConfig.TARGET_PENG, 3.9663293f, 0.99100536f, 0.5f));
		group.addActor (new PengActor(world, MaterialConfig.TARGET_PENG, 3.907611f, 1.812549f, 0.5f));
		group.addActor (new PengActor(world, MaterialConfig.TARGET_PENG, 2.0906248f, 0.30906194f, 0.5f));
		group.addActor (new PengActor(world, MaterialConfig.TARGET_PENG, 5.294021f, 0.9922342f, 0.5f));
		group.addActor (new PengActor(world, MaterialConfig.TARGET_PENG, 3.8892386f, 2.619727f, 0.5f));

		return group;
	}

}
```

After putting it in the source code I can restart the game and see how it looks like. Now it will have the look that the user has, the content will be on both sides.
![image](uploads/9315c55108d107987ed8586c156ce4e7/image.png)

# play it (test playablility)
When you are done creating all your levels, turn the sandbox mode off again in ``./core/src/com/gitlab/ardash/appleflinger/global/GameManager.java``:

```
	public static final boolean SANDBOX = false;
```
Restart the game and play it **in 2-player-mode**. Make sure:
*  the level starts in a stable mode, it should not destroy itself, while it starts
*  can be played and can be won. enemies should not be obstructed in a way, that is is impossible to win
*  It not to hard and not to easy. If you create a whole episode of levels, put the easier levels first and the harder ones last. That way the user won't have to give up right at the start. Remember: The user can only unlock the next level, by winning the current level once!

# publish it
At this stage the level can already be send out, to be added to the game. Here is a bit more info:

1.  Put the current year and your name and email address in the first line of the header of your file. That means the copyright is in your name. Remove all other copyright names there if you created the level all by yourself. But don't change the license text in the file.
2.  Since Apple Flinger is licensed under GPL3+, all changes, additions, ad-ons, forks, levels, etc .... must also be GPL3+. That means you can't change the license in the file and you can't release your own version of the game without releasing the source code.
3.  If you want your levels included in the original Apple Flinger game (which is highly encouraged), please send a merge request on Gitlab (or a pull request on Github, if you have to).
4.  Alternatively you can also send the files in an email the email address in the README.md file.
5.  You don't need to write additional code. If you have created a whole episode, just let me know, what name you would like to have for it, and I will put it all in place for you.
6.  You will automatically be added to the AUTHORS.md file and the credits-screen in game. Just let me know, if you don't want that or if you would like the be mentioned with a particular name. Please provide also a representation in Latin based letters (for Chinese or Cyrillic names), so it can be easily displayed on the credits-screen.

# [optional] teach the computer to play the level
This will be done by someone else, but you can also do it. The current implementation of the AI (computer enemy) is the record the shots that kill the enemies. The AI will replay them later, with some small changes to add some variety.

## record shots
To do that set in ``./core/src/com/gitlab/ardash/appleflinger/global/GameManager.java``:

```
	public static final boolean RECORDSHOTS = true;
```

Then play the level in the following way:
*  2-player mode
*  player 1: shoots all shots to the left (so he doesn't hit anything)
*  player 2: shoots all enemies from top to bottom. Hit them in their unchanged position (before they fall down, or move somewhere else)
*  after winning play it another 2 or 3 times, more recorded shots are better, so the AI is not too dumb. 
*  hit enemies from different angles and in different ways: high-shots and low-shots

In the CLI you will see that all shots, that hit an enemy are printed. Copy all the lines out there and paste them into the file 
```
./core/src/com/gitlab/ardash/appleflinger/ai/HCStrategy.java
```

In this tutorial I created  the level `M_2_7` so i paste them right below all the shots for `M_2_6`. The file is straight forward.

## test it
Restart the game but this time, click on *alone*. So you play against the computer. The computer will play a variety of your recorded shots. Test it this way
*  player 1: shoots all shots to the left (so he doesn't hit anything)
*  player 2: should win the game after a while (not too slowly or quickly)
*  make sure there is no situation, there the computer just shoots into the room, without trying to hit the enemy, if that happens you didn't record a shot for an enemy in that location, have have to start 2-player-mode again and record it
*  restart and actually try to beat the computer, it must not be too hard or too easy. If the computer can't be beaten (is too good) the level cannot be released like that. if the computer never wins, try to record some more (better) shots.